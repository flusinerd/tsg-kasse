import { Injectable } from '@nestjs/common';
import { UsersService } from 'src/users/services/users.service';
import { UserDto } from 'src/users/model/dto/user.dto';
import { compareSync } from 'bcrypt'
import { User } from 'src/users/model/user.entity';
import { TokenResponse } from './tokenResponse';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthenticationService {
  constructor(private readonly usersService: UsersService, private readonly _jwtService: JwtService){}

  async validateUser(username: string, password: string): Promise<UserDto>{
    const user = await this.usersService.findOneByUsername(username);
    if (user && compareSync(password, user.password)){
      const { password, ...result } = user;
      return result;
    }
    return null;
  }

  async login(user: User): Promise<TokenResponse>{
    const payload = {
      username: user.username,
      sub: user.id
    }

    return {
      access_token: this._jwtService.sign(payload)
    }
  }
}
