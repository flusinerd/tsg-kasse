import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from './users/users.module';
import { PassportModule } from '@nestjs/passport';
import { AuthenticationModule } from './authentication/authentication.module';
import { GraphQLModule } from '@nestjs/graphql';

@Module({
  imports: [TypeOrmModule.forRoot({
    type: 'mysql',
    username: 'RlwjqYVY78',
    password: 'Pw4RlwjqYVY78#123',
    host: 'nw-system.de',
    port: 3306,
    database: 'RlwjqYVY78',
    synchronize: true,
    entities: [__dirname + "/**/*.entity{.ts,.js}"]
  }), UsersModule,
  PassportModule,
  AuthenticationModule,
  GraphQLModule.forRoot({
    autoSchemaFile: 'schema.gql',
    // autoSchemaFile: true,
    playground: true
  }),],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
