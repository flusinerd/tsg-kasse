import { ArgsType, Field, Int } from "type-graphql";
import { Min } from "class-validator";

@ArgsType()
export class UsersArgs {
  @Field(type => Int)
  @Min(0)
  skip = 0;

}