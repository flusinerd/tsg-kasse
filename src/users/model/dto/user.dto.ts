export class UserDto {
  password?: string;
  id: number;
  username: string;
  name: string;
  fullname: string;
  admin: boolean;
  balance: number;
  rfid: string; 
}