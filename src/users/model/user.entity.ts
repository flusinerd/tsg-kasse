import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";
import { ObjectType, Field } from "type-graphql";

@ObjectType()
@Entity()
export class User {
  @Field()
  @PrimaryGeneratedColumn()
  id: number;

  @Field({ nullable: true })
  @Column({ nullable: true })
  name: string;

  @Field({ nullable: true })
  @Column({ nullable: true })
  fullname: string;

  @Field()
  @Column({readonly: true, nullable: false, unique: true})
  username: string;

  @Column({ nullable: false })
  password?: string;

  @Field()
  @Column({default: false})
  admin: boolean;

  @Field()
  @Column({ default: 0, type: 'int' })
  balance: number;

  @Field({ nullable: true })
  @Column({nullable: true})
  rfid: string;
}