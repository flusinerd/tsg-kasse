import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../model/user.entity';
import { Repository } from 'typeorm';
import { hashSync } from 'bcrypt';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly _userRepository: Repository<User>,
  ) {}

  async findAll(): Promise<User[]> {
    return this._userRepository.find();
  }

  async findOne(id: string|number): Promise<User> {
    return this._userRepository.findOne(id);
  }

  async findOneByUsername(username: string): Promise<User> {
    return this._userRepository.findOne({ where: { username } });
  }

  async createOne(newUser: User): Promise<User> {
    if (!newUser.password || !newUser.username) {
      throw new Error("Incomplete")
    }
    const record = await this._userRepository.findOne({
      where: { username: newUser.username },
    });
    if (record === undefined) {
      newUser.password = hashSync(newUser.password, 10);
      const created = await this._userRepository.save(newUser);
      return created;
    } else {
      return;
    }
  }

  async updateOne(id: string, updatedUser: User): Promise<User> {
    await this._userRepository.findOneOrFail(id);

    // Hash password if updated
    if (updatedUser.password){
      updatedUser.password = hashSync(updatedUser.password, 10)
    }
    updatedUser.id = Number(id);
    await this._userRepository.save(updatedUser);
    return await this._userRepository.findOne(id);
  }
}
