import { Resolver, Query, Args } from "@nestjs/graphql";
import { User } from "../model/user.entity";
import { UsersService } from "../services/users.service";
import { Int } from "type-graphql";

@Resolver(of => User)
export class UserResolver {
  constructor(
    private readonly _usersService: UsersService,
  ) {}

  @Query(returns => User)
  async user(@Args({ name: 'id', type: () => Int }) id: number): Promise<User>{
    console.log('hit')
    return this._usersService.findOne(id);
  }

  @Query(returns => [User])
  async users(): Promise<User[]>{
    return this._usersService.findAll();
  }
}