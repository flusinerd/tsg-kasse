import { Controller, Post, Body, Put, Param, Get, UseGuards } from '@nestjs/common';
import { User } from '../model/user.entity';
import { UsersService } from '../services/users.service';
import { JwtAuthGuard } from 'src/authentication/jwt-auth.guard';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}
  
  @Post()
  async createOne(@Body() user: User){
    const returnvalue = await this.usersService.createOne(user)
    return returnvalue;
  }

  @Put(':id')
  async updateOne(@Param('id') id: string, @Body() updatedUser: User){
    return this.usersService.updateOne(id, updatedUser);
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  async getAll(){
    return this.usersService.findAll();
  }
}
