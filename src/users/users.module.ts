import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './model/user.entity';
import { UsersService } from './services/users.service';
import { UsersController } from './controller/users.controller';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from 'src/authentication/constants';
import { UserResolver } from './controller/users.resolver';

@Module({
  imports: [TypeOrmModule.forFeature([User]), PassportModule,
  JwtModule.register({
    secret: jwtConstants.secret,
    signOptions: { expiresIn: "1d" },
  }),
],
  providers: [UsersService, UserResolver],
  controllers: [UsersController],
  exports: [UsersService]
})
export class UsersModule {}
